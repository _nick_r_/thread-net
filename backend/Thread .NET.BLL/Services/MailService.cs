﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    class MailService : BaseService
    {
        public MailService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        static MailAddress from = new MailAddress("", "Thread Net");

        public static void SendLikeEmail(string mailString)
        {
            MailAddress to = new MailAddress(mailString);
            MailMessage mail = new MailMessage(from, to);
            mail.Subject = "Like";
            mail.Body = "Somebody liked your post";
            mail.IsBodyHtml = false;
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.Credentials = new NetworkCredential("", "");
            smtp.EnableSsl = true;
            smtp.Send(mail);


        }

    }
}