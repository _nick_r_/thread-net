import { Component, Input, OnDestroy } from '@angular/core';
import { Reaction } from '../../models/reactions/reaction';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { Console } from 'console';

@Component({
  selector: 'app-reaction',
  templateUrl: './reaction.component.html',
  styleUrls: ['./reaction.component.sass']
})
export class ReactionComponent {

  @Input() public reaction: Reaction;
  @Input() public currentUser: User;

  public constructor(
    private authService: AuthenticationService,
    private authDialogService: AuthDialogService,
    private likeService: LikeService,
    private commentService: CommentService,
    private snackBarService: SnackBarService
  ) { }


}
