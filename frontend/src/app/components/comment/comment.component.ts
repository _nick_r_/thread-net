import { Component, Input, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { Console } from 'console';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    public showReactions = false;
    public showEditContainer = false;

    private unsubscribe$ = new Subject<void>();

    public editedComment = {} as NewComment;

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleEditContainer() {
        this.showEditContainer = !this.showEditContainer;
    }

    public toggleReactions() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showReactions = !this.showReactions;
                    }
                });
            return;
        }

        this.showReactions = !this.showReactions;
    }

    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public countLikes() {
        var likes = this.comment.reactions.filter((x) => x.isLike == true);
        return likes.length;
    }

    public countDislikes() {
        var likes = this.comment.reactions.filter((x) => x.isLike == false);
        return likes.length;
    }

    public deleteComment() {
        this.commentService
            .deleteComment(this.comment.id).pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {
                window.location.reload();
                this.snackBarService.showUsualMessage('Successfully deleted');
            });
    }

    public editComment() {
        this.commentService
            .EditComment(this.comment).pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {
                //window.location.reload();
                this.snackBarService.showUsualMessage('Successfully edited');
                this.showEditContainer = false;
            });
    }


    public isAuthorOfComment() {
        try {
            if (this.currentUser.id === this.comment.author.id) {
                return false;
            } else {
                return true;
            }
        } catch (e: unknown) {

            if (typeof e === "string") {
                e.toUpperCase() // works, `e` narrowed to string
            } else if (e instanceof Error) {
                console.log(e.message) // works, `e` narrowed to Error

            }
        }
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

}
